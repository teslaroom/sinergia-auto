var jQueryScriptOutputted = false;
var callback_silence = false;
var callback_in_progress = false;
var callback_api = '//callback.deskstore.ru/api.php';
//var callback_api = 'http://callback.local/api.php';
var show_on_leave = true;
var getCookie, setCookie, openWidget, changePage;

getCookie = function (name)
{
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
};

setCookie = function (name, value, options)
{
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
};

function initJQuery()
{

    if (typeof(jQuery) == 'undefined') {

        if (!jQueryScriptOutputted) {
            jQueryScriptOutputted = true;

            var script = document.createElement('script');
            script.src = "//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js";
            script.onload = function(){
                initJQuery();
            };
            script.async = false;
            document.head.appendChild(script);
            //script.onload(function () {
                //initJQuery();
            //})

        }
    } else {
        var $widget_layer;

        openWidget = function ()
        {
            $widget_layer.addClass('open');
            jQuery('input[name=where]', $widget_layer).val('');
        };

        changePage = function (page)
        {
            show_on_leave = false;

            if (callback_in_progress)
                return;

            if (!page)
                return;

            jQuery('#callback_widget [data-page=' + page + ']')
                .show()
                .siblings('[data-role=page]')
                .hide()
            ;
        };

        jQuery(function ()
        {
            var cookie_name = 'callback_params';
            var control_key = false;

            if (!getCookie(cookie_name))
                setCookie(cookie_name, location.search,
                    {
                        expires: 3600 * 24 * 30,
                        path: '/'
                    });

            if (callback_key) {
                jQuery.ajax({
                    url: callback_api + '/card/get',
                    jsonp: "callback",
                    dataType: "jsonp",
                    data: {
                        key: callback_key,
                        host: location.host,
                        uri: location.pathname,
                        params: getCookie(cookie_name)
                    },
                    success: function (response)
                    {
                        if (console !== undefined)
                            console.log(response);

                        jQuery('body').append(response.html);

                        $widget_layer = jQuery('#callback_widget');
                        var $widget = jQuery('#callback_widget_tel');
                        var $widget_circle = jQuery('> :nth(0)', $widget);
                        var $widget_circle_fill = jQuery('> :nth(1)', $widget);

                        var circle_animate = function ()
                        {
                            setTimeout(function ()
                            {
                                $widget_circle.addClass('big');

                                setTimeout(function ()
                                {
                                    $widget_circle.addClass('fade');

                                    setTimeout(function ()
                                    {
                                        $widget_circle.removeClass();
                                        circle_animate();
                                    }, 4000);
                                }, 600);
                            }, 1000);
                        };

                        var circle_fill_animate = function ()
                        {
                            setTimeout(function ()
                            {
                                $widget_circle_fill.addClass('big');

                                setTimeout(function ()
                                {
                                    $widget_circle_fill.removeClass('big');
                                    circle_fill_animate();
                                }, 700);
                            }, 2000);
                        };

                        circle_animate();
                        circle_fill_animate();

                        $widget.click(function ()
                        {
                            changePage(6);
                            openWidget();
                        });

                        jQuery('#callback_widget [type="tel"]').mask('+7 (999) 999-99-99');

                        jQuery('#callback_widget input')
                            .focus(function ()
                            {
                                jQuery(this).removeClass('error');
                            })
                            .keypress(function (e)
                            {
                                if (e.which == 13) {
                                    trySubmitForm(jQuery(this));
                                    console.log('You pressed enter!', jQuery(this).val());
                                }
                            });

                        jQuery('#callback_widget button')
                            .click(function ()
                            {
                                trySubmitForm(jQuery(this));
                            });

                        jQuery('#callback_widget a.button, #callback_widget a.link')
                            .click(function ()
                            {
                                if (!jQuery(this).attr('data-next'))
                                    $widget_layer.removeClass('open');

                                changePage(jQuery(this).attr('data-next'));
                            });

                        $widget_layer.click(function (e)
                        {
                            if (jQuery(e.target).closest('.callback_inner').length == 0 || jQuery(e.target).is('.callback_close')) {
                                jQuery(this).removeClass('open');
                            }
                        });

                        jQuery('#callback_widget .fail').click(function ()
                        {
                            jQuery.ajax({
                                url: callback_api + '/card/repeat',
                                jsonp: "callback",
                                dataType: "jsonp",
                                data: {
                                    key: control_key
                                },
                                success: function (response)
                                {
                                }
                            });
                        });

                        if (response.is_open && response.countdown && !getCookie('callback_first_time')) {
                            setTimeout(function ()
                            {
                                if (!callback_silence) {
                                    changePage(1);
                                    openWidget();
                                }

                                setCookie('callback_first_time', true,
                                    {
                                        expires: 3600,
                                        path: '/'
                                    });

                            }, response.countdown * 60000);
                        }
                    }
                });

                var hexString = function (str){
                    str = str.replace(/\\u([0-9a-f]{4})/g, function($0, $1)
                    {
                        return String.fromCharCode(parseInt("0x" + $1));
                    });
                    return str;
                };

                var trySubmitForm = function ($child)
                {
                    if (callback_in_progress)
                        return;

                    var ready = true;
                    var $inputs = $child.parent().find('input[type!=hidden]');

                    $inputs.blur();

                    $inputs.each(function ()
                    {
                        if (!jQuery(this).val() && jQuery(this).attr('required')) {
                            jQuery(this).addClass('error');
                            ready = false;
                        }
                    });

                    if (ready) {
                        changePage($inputs.filter(':first').attr('data-next'));

                        var callbackCountdown = 60;
                        var callbackInterval = setInterval(function ()
                        {
                            var cases = [2, 0, 1, 1, 1, 2];
                            var titles = ['\u0441\u0435\u043A\u0443\u043D\u0434\u0430', '\u0441\u0435\u043A\u0443\u043D\u0434\u044B', '\u0441\u0435\u043A\u0443\u043D\u0434'];
                            var number = callbackCountdown;

                            jQuery('#callback_countdown').text(callbackCountdown + ' ' + hexString(titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]]));

                            callbackCountdown--;
                            if (!callbackCountdown) {
                                callback_in_progress = false;
                                clearInterval(callbackInterval);
                                changePage(5);
                            }
                        }, 1000);

                        var data = {};

                        data['key'] = callback_key;
                        data['host'] = location.host;
                        data['uri'] = location.pathname;
                        data['params'] = getCookie(cookie_name);

                        $child.parent().find('input').each(function ()
                        {
                            data[jQuery(this).attr('name')] = jQuery(this).val();
                        });

                        console.log(data);

                        jQuery.ajax({
                            url: callback_api + '/card/push',
                            jsonp: "callback",
                            dataType: "jsonp",
                            data: data,
                            success: function (response)
                            {
                                if (console !== undefined)
                                    console.log(response);

                                control_key = response.control_key;

                                if (response.inner_id) {
                                    jQuery('[data-page=4] p:last').html('<br/>Номер вашего заказа ' + response.inner_id);
                                }
                            }
                        });

                        $inputs.each(function ()
                        {
                            jQuery(this).val('');
                        });

                        callback_in_progress = true;
                    }
                };

            }

        });


        /*
         Masked Input plugin for jQuery
         Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
         Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
         Version: 1.3.1
         */
        (function (e)
        {
            function t()
            {
                var e = document.createElement("input"), t = "onpaste";
                return e.setAttribute(t, ""), "function" == typeof e[t] ? "paste" : "input"
            }

            var n, a = t() + ".mask", r = navigator.userAgent, i = /iphone/i.test(r), o = /android/i.test(r);
            e.mask = {
                definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
                dataName: "rawMaskFn",
                placeholder: "_"
            }, e.fn.extend({
                caret: function (e, t)
                {
                    var n;
                    if (0 !== this.length && !this.is(":hidden"))return "number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function ()
                    {
                        this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (n = this.createTextRange(), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
                    })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
                        begin: e,
                        end: t
                    })
                }, unmask: function ()
                {
                    return this.trigger("unmask")
                }, mask: function (t, r)
                {
                    var c, l, s, u, f, h;
                    return !t && this.length > 0 ? (c = e(this[0]), c.data(e.mask.dataName)()) : (r = e.extend({
                        placeholder: e.mask.placeholder,
                        completed: null
                    }, r), l = e.mask.definitions, s = [], u = h = t.length, f = null, e.each(t.split(""), function (e, t)
                    {
                        "?" == t ? (h--, u = e) : l[t] ? (s.push(RegExp(l[t])), null === f && (f = s.length - 1)) : s.push(null)
                    }), this.trigger("unmask").each(function ()
                    {
                        function c(e)
                        {
                            for (; h > ++e && !s[e];);
                            return e
                        }

                        function d(e)
                        {
                            for (; --e >= 0 && !s[e];);
                            return e
                        }

                        function m(e, t)
                        {
                            var n, a;
                            if (!(0 > e)) {
                                for (n = e, a = c(t); h > n; n++)if (s[n]) {
                                    if (!(h > a && s[n].test(R[a])))break;
                                    R[n] = R[a], R[a] = r.placeholder, a = c(a)
                                }
                                b(), x.caret(Math.max(f, e))
                            }
                        }

                        function p(e)
                        {
                            var t, n, a, i;
                            for (t = e, n = r.placeholder; h > t; t++)if (s[t]) {
                                if (a = c(t), i = R[t], R[t] = n, !(h > a && s[a].test(i)))break;
                                n = i
                            }
                        }

                        function g(e)
                        {
                            var t, n, a, r = e.which;
                            8 === r || 46 === r || i && 127 === r ? (t = x.caret(), n = t.begin, a = t.end, 0 === a - n && (n = 46 !== r ? d(n) : a = c(n - 1), a = 46 === r ? c(a) : a), k(n, a), m(n, a - 1), e.preventDefault()) : 27 == r && (x.val(S), x.caret(0, y()), e.preventDefault())
                        }

                        function v(t)
                        {
                            var n, a, i, l = t.which, u = x.caret();
                            t.ctrlKey || t.altKey || t.metaKey || 32 > l || l && (0 !== u.end - u.begin && (k(u.begin, u.end), m(u.begin, u.end - 1)), n = c(u.begin - 1), h > n && (a = String.fromCharCode(l), s[n].test(a) && (p(n), R[n] = a, b(), i = c(n), o ? setTimeout(e.proxy(e.fn.caret, x, i), 0) : x.caret(i), r.completed && i >= h && r.completed.call(x))), t.preventDefault())
                        }

                        function k(e, t)
                        {
                            var n;
                            for (n = e; t > n && h > n; n++)s[n] && (R[n] = r.placeholder)
                        }

                        function b()
                        {
                            x.val(R.join(""))
                        }

                        function y(e)
                        {
                            var t, n, a = x.val(), i = -1;
                            for (t = 0, pos = 0; h > t; t++)if (s[t]) {
                                for (R[t] = r.placeholder; pos++ < a.length;)if (n = a.charAt(pos - 1), s[t].test(n)) {
                                    R[t] = n, i = t;
                                    break
                                }
                                if (pos > a.length)break
                            } else R[t] === a.charAt(pos) && t !== u && (pos++, i = t);
                            return e ? b() : u > i + 1 ? (x.val(""), k(0, h)) : (b(), x.val(x.val().substring(0, i + 1))), u ? t : f
                        }

                        var x = e(this), R = e.map(t.split(""), function (e)
                        {
                            return "?" != e ? l[e] ? r.placeholder : e : void 0
                        }), S = x.val();
                        x.data(e.mask.dataName, function ()
                        {
                            return e.map(R, function (e, t)
                            {
                                return s[t] && e != r.placeholder ? e : null
                            }).join("")
                        }), x.attr("readonly") || x.one("unmask", function ()
                        {
                            x.unbind(".mask").removeData(e.mask.dataName)
                        }).bind("focus.mask", function ()
                        {
                            clearTimeout(n);
                            var e;
                            S = x.val(), e = y(), n = setTimeout(function ()
                            {
                                b(), e == t.length ? x.caret(0, e) : x.caret(e)
                            }, 10)
                        }).bind("blur.mask", function ()
                        {
                            y(), x.val() != S && x.change()
                        }).bind("keydown.mask", g).bind("keypress.mask", v).bind(a, function ()
                        {
                            setTimeout(function ()
                            {
                                var e = y(!0);
                                x.caret(e), r.completed && e == x.val().length && r.completed.call(x)
                            }, 0)
                        }), y()
                    }))
                }
            })
        })(jQuery);

    }

}
setTimeout(initJQuery, 3000);
