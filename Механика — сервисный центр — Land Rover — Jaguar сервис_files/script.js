var callRecaptcha;
var servicesRecaptcha;

// var onloadRecaptcha = function() {
//   servicesRecaptcha = grecaptcha.render('services-recaptcha', {
//     'sitekey': '6LcJ4CoUAAAAABfyCzlDVldMTNV20yxqb_w8CyhP',
//     'theme': 'light'
//   });
// };

(function($) {
  $(document).ready(function() {

    // Flush triggers
    $(document).on('click touchend', function(e) {
      var triggerHamburger = $(e.target).parents('.trigger-hamburger').length;

      if(!triggerHamburger) {
        $('.hamburger-menu-wrapper').removeClass('opened');
      }
    });

    // Hamburger
    $('.trigger-hamburger').off('click');
    $('.trigger-hamburger').on('click', function(e) {
      e.preventDefault();
      $('.hamburger-menu-wrapper').addClass('opened');
    });

    // Mask phone number
    $('input.phone').mask('+7 (999) 999-99-99');

    // Custom scrollbar
    $(".checkboxes-group").mCustomScrollbar({theme: 'dark'});

    // Tabs
    $(document).on('click', '.tab', function(e) {
      var target = $(this).attr('data-target');
      $(this).parents('.tabs-wrapper').find('.tab').removeClass('active');
      $(this).parents('.tabs-wrapper').find('.tab[data-target="'+target+'"]').addClass('active');

      $(this).parents('.tabs-wrapper').find('.tab').each(function() {
        var target = $(this).attr('data-target');
        $(target).removeClass('active');
      });

      $(target).addClass('active');
    });

    // Switches
    $(document).on('click', '.switches', function(e) {
      var target = $(e.target).attr('data-target');
      $(this).find('.switch').removeClass('active');
      $(this).find('.switch[data-target="'+target+'"]').addClass('active');

      $(this).find('.switch').each(function() {
        var target = $(this).attr('data-target');
        $(target).removeClass('active');
      });

      $(target).addClass('active');
    });

    // Slideshow
    $('.slideshow-inner').slick({dots: true, autoplay: true, autoplaySpeed: 5000, appendArrows: '.slideshow-dots', responsive: [
      {
        breakpoint: 1400,
        settings: {
          arrows: false
        }
      },
      {
        breakpoint: 1229,
        settings: {
          arrows: true
        }
      },
      {
        breakpoint: 1120,
        settings: {
          arrows: false
        }
      }
    ]});

    // Services Carousel
    $('.services-carousel').slick({dots: true, slidesToShow: 3, slidesToScroll: 3, infinite: false, responsive: [
      {
        breakpoint: 1400,
        settings: {
          arrows: false
        }
      },
      {
        breakpoint: 1229,
        settings: {
          arrows: true
        }
      },
      {
        breakpoint: 1120,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]});

    // Reviews Carousel
    $('.reviews-carousel').slick({dots: true, slidesToShow: 2, slidesToScroll: 2, infinite: false, responsive: [
      {
        breakpoint: 1400,
        settings: {
          arrows: false
        }
      },
      {
        breakpoint: 1229,
        settings: {
          arrows: true
        }
      },
      {
        breakpoint: 1120,
        settings: {
          arrows: false
        }
      },
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]});

    // Benefits Carousel
    $('.benefits-carousel').slick({dots: true, slidesToShow: 3, slidesToScroll: 3, infinite: false, responsive: [
      {
        breakpoint: 1400,
        settings: {
          arrows: false
        }
      },
      {
        breakpoint: 1229,
        settings: {
          arrows: true
        }
      },
      {
        breakpoint: 1120,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]});

    // Smooth scroll
    $('a[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top - 50
          }, 500);
          return false;
        }
      }
    });

    // Return call form
    $('#call-form').on('shown.bs.modal', function () {
      // if(typeof callRecaptcha === 'undefined') {
      //   callRecaptcha = grecaptcha.render('call-recaptcha', {
      //     'sitekey': '6LcJ4CoUAAAAABfyCzlDVldMTNV20yxqb_w8CyhP',
      //     'theme': 'light'
      //   });
      // }
    })
    $('#mechanica-call-form').on('submit', function(e) {
      e.preventDefault();
      var name = $(this).find('#form-call-name').val();
      var phone = $(this).find('#form-call-phone').val();
      // var reCaptcha = grecaptcha.getResponse(callRecaptcha);
      var ajax_url = ajax_params.ajax_url;

      var data = {
        'action': 'mechanica_form',
        'title': 'Обратный звонок от '+ name +': ' + phone,
        'content': '',
        'category': '5'
      };

      // if(reCaptcha) {
        $.post(ajax_url, data, function(response) {
          console.log(response);
          if (~response.indexOf('success')) {
            yaCounter45809346.reachGoal('call');
            $('#mechanica-call-form').hide();
            $('#form-call-message').show();
          }
        });
      // }
    });

    // Subscription form
    var subscribed = localStorage.getItem('subscribed');
    if (subscribed) {
      $('#subscription-block').hide();
    }
    $('#form-subscription').on('submit', function(e) {
      e.preventDefault();
      var email = $(this).find('input').val();
      var ajax_url = ajax_params.ajax_url;
      var data = {
        'action': 'mechanica_form',
        'title': 'Подписка: ' + email,
        'content': '',
        'category': '3'
      };
      $.post(ajax_url, data, function(response) {
        if (~response.indexOf('success')) {
          yaCounter45809346.reachGoal('subscription');
          $('#subscription-block').hide();
          $('#subscription-form-message').show();
          localStorage.setItem('subscribed', '1');
        }
      });
    });

    // Services form
    if($('#services-form').length) {
      let $form = $('#services-form');

      // Listens to model name selection to serve appropiate services checkboxes
      $form.find('.model-name').on('change', function() {
        let $tab = $(this).parents('.tab-content');
        $tab.find('.custom-services .checkboxes-group').removeClass('active');
        $tab.find('.custom-services .checkboxes-group[data-model="'+$(this).val()+'"]').addClass('active');
        $tab.find('.car-image img').attr('src', ajax_params.template_uri + '/images/' + $(this).find('option:selected').attr('data-image'));
      });

      $form.on('submit', function(e) {
        e.preventDefault();
        let $tab = $form.find('.tab-content.active');
        let vendor = $form.find('.tab.active').text().trim();
        let model = $tab.find('.model-name').val();
        let fullname = $('#form-services-full-name').val();
        let phone = $('#form-services-phone').val();
        let title = '';
        let content = '';
        let customServices = '';
        let generalServices = '';
        let message = '';

        $tab.find('.custom-services .checkboxes-group.active input').each(function() {
          if($(this).prop('checked')) {
            customServices += '-' + $(this).val() + '\r\n';
          }
        });

        $tab.find('.general-services .checkboxes-group input').each(function() {
          if($(this).prop('checked')) {
            generalServices += '-' + $(this).val() + '\r\n';
          }
        });

        if (phone) {
          if (customServices || generalServices) {
            title = 'Заявка от ' + fullname + '\r\n';
            content += 'Имя: ' + fullname + '\r\n';
            content += 'Телефон: ' + phone + '\r\n';
            content += vendor + ': ' + model + '\r\n';
            if (customServices) {
              content += 'Услуги ' + model + ':\r\n';
              content += customServices + '\r\n';
            }
            if (generalServices) {
              content += 'Общие услуги: \r\n';
              content += generalServices + '\r\n';
            }

            // Send form to the server
            // let reCaptcha = grecaptcha.getResponse(servicesRecaptcha)
            // if(reCaptcha) {
              let ajax_url = ajax_params.ajax_url;
              let data = {
                'action': 'mechanica_form',
                'title': title,
                'content': content,
                'category': '4',
                'notification': '1'
              };
              $.post(ajax_url, data, function(response) {
                if (~response.indexOf('success')) {
                  yaCounter45809346.reachGoal('order');
                  $('#services-form').hide();
                  $('#services-form-message').show();
                }
              });

          } else {
            message = 'Необходимо выбрать хотя бы одну услугу';
            $('#modal-message').modal('show');
            $('.error-message').text(message);
          }
        } else {
          message = 'Укажите телефон';
          $('#modal-message').modal('show');
          $('.error-message').text(message);          
        }
      });
    }

    // Duplicates form
    if($('#duplicates-form').length) {
      let $form = $('#duplicates-form');

      $form.on('submit', function(e) {
        e.preventDefault();
        let fullname = $('#form-duplicates-name').val();
        let phone = $('#form-duplicates-phone').val();
        let title = 'Заявка на подбор запчастей от ' + fullname + '\r\n';
        let content = $('#form-duplicates-text').val();
        let message = '';

        if (phone) {
              let ajax_url = ajax_params.ajax_url;
              let data = {
                'action': 'mechanica_form',
                'title': title,
                'content': content,
                'category': '6',
                'notification': '2'
              };
              $.post(ajax_url, data, function(response) {
                if (~response.indexOf('success')) {
                  yaCounter45809346.reachGoal('order');
                  $('#duplicates-form').hide();
                  $('#duplicates-form-message').show();
                }
              });
        } else {
          message = 'Укажите телефон';
          $('#modal-message').modal('show');
          $('.error-message').text(message);          
        }
      });
    }

  });
}(jQuery));